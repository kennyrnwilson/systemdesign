# System Design

This project contains 

 - Building Blocks -Single Nodes such as SQLServer or API
 - Scenarions - Combinations of Nodes

 A lot of the nodes and scenarion use docker and docker-compose but some also run locally as standalone services. 

## Building Block Catalogue 

### SQL Server

| Link        | Description           | 
| ------------- |:-------------:| 
| [Empty Server](./buildingblocks/sqlserver/server-empty)  |Bring up an empty SQL Server instance | 
| [Server and Data](./buildingblocks/sqlserver/server-with-data) |SQL Server + Simple Tables | 

<br>

### KeyCloak

| Link        | Description           | 
| ------------- |:-------------:| 
| [Server and Realm](./buildingblocks/keycloak/server-and-realm)  | KeyCloak plus simple user and client
 | 

 <br>

### .NET Core
| Link        | Description           | 
| ------------- |:-------------:| 
| [Bare bones Web Server](./buildingblocks/aspdotnetcore/1.%20Basic%20Example)  | Hello World Web Server
 | 
| [Serilog](./buildingblocks/aspdotnetcore/2.%20MappingDirectory/ASPHelloWorld)  | Serilog plus docker volume mapping
 | 
| [HTTPS](./buildingblocks/aspdotnetcore/3.%20DockerAndHttps)  | Web API Over HTTP on Docker
 | 

<br>

### GraphQL

| Link        | Description           | 
| ------------- |:-------------:| 
| [Node Server](./buildingblocks/graphql/node/template-project)  | Template for Node.JS server running in express using TypeScript | 

<br>

### ELK (Elastic, Logstash, Kibana)

| Link        | Description           | 
| ------------- |:-------------:| 
| [Basic ELK Stack](./buildingblocks/elk/basic-elk-stack)  | Template for starting up ELK stack in docker | 

<br>

## Scenario Catalogue 

| Link        | Description           | 
| ------------- |:-------------:| 
| [Client/Server Auth](./distributed-scenarios/client-server-authenticated)  |SPA, WebAPI plus Auth Server | 
