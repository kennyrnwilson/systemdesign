## Run and populate KeyCloak?
The following command will run up a KeyCloak server in a docker container with the following properties

- Exposes **localhost:8080** so we can access from the host 
- Setups an **admin** account with **password** admin
- Imports a real called **kennysrealm** from the config in the subdirectory **./kcdata**
- Mounts the local file *./kcdata* to the */tmp/kcdata* folder on the container so we can import from *./kcdata*

```
docker run -p 8080:8080 -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin -e KEYCLOAK_IMPORT=/tmp/kcdata/kennyrealm-realm.json   -v ./kcdata:/tmp/kcdata/ --name kc jboss/keycloak
```

## Export any changes made in management console
You need to export the data to the ./kcdata folder as follows.

**Note:** If you run the command and get errors it might be you need to change the **-Djboss.socket.binding.port-offset=100** to something other that 100

```
docker exec -it kc /opt/jboss/keycloak/bin/standalone.sh -Djboss.socket.binding.port-offset=100 -Dkeycloak.migration.action=export -Dkeycloak.migration.provider=singleFile -Dkeycloak.migration.realmName=kennyrealm -Dkeycloak.migration.usersExportStrategy=REALM_FILE -Dkeycloak.migration.file=/tmp/kcdata/kennyrealm-realm.json

```

If the above command does not copy the file over the volume to the host directory we can copy it as follows

```
docker cp kc:/tmp/kcdata/kennyrealm-realm.json .
```

