import React, { ReactElement } from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
import Keycloak from 'keycloak-js';

const keycloak = Keycloak(`/config/keycloak.json?ts=${new Date().getTime()}`);

async function DoWork() {
  await keycloak.init(
    {
      onLoad: 'login-required',
      enableLogging: true,
      checkLoginIframe: false
    });

  await keycloak.updateToken(120);
  const token = keycloak.token;
  const tokenParsed = keycloak.tokenParsed;

  const result = await fetch('http://localhost:8050/weatherforecast',
    {
      mode: "cors",
      headers: [
        ['authorization', `Bearer ${keycloak.token}`]
      ]
    }
  );

  ReactDOM.render(
    <React.StrictMode>
      <App json={await result.json()}></App>
    </React.StrictMode>,
    document.getElementById('root')
  );
}


function App(props:any) : ReactElement
{
  return <pre>{JSON.stringify(props.json,null, 2)}</pre>
}

DoWork();
