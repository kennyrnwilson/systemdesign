# Authenticated  Client / Server

This scenario brings up an instance of a web client and a .NET Core server that authenticates using KeyCloak.

 - Client : REACT.JS
 - Server: .NET Core
 - Auth Server : KeyCloak

## Topology
![alt text](./docs/images/topology.png "Topology")


## Prerequisite
To make this example work you need to add an entry to the windows host file *C:\Windows\System32\drivers\etc\hosts* This is is because the .NET server in it's container can only access the KC docker container via the kc link so we need to also be able to access this from the windows machine

```
127.0.0.1       kc
```
## Run
```
docker-compose up --build
```

## Nodes

### KeyCloak

| Syntax        | Description                 |
| -----------   | ----------------------------|
| URL           | http://localhost:8080       |
| Admin User    | admin                       |
| Admin Pword   | admin                       |
| User          | testuser                    |
| Password      | testuser                    |

<br>

### REACT.js
When you open the react app on http://localhost:3000 and enter the username: testuser and password:testuser


### .DOTNET Server Swagger
http://localhost:8050/swagger/index.html
