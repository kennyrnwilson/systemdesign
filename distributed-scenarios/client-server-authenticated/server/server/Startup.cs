using System;
using System.Collections;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace server
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            foreach (DictionaryEntry environmentVariable in Environment.GetEnvironmentVariables())
            {
                Console.WriteLine($"{environmentVariable.Key} : {environmentVariable.Value}");
            }
            services.AddCors();

            services.AddControllers();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "server", Version = "v1" });
            });

            var auth = services.AddAuthentication();

            string realm = _configuration.GetValue<string>("KeyCloak:Realm");
            string audience = "testclient";

            Console.WriteLine($"Using Authority {realm} ");
            Console.WriteLine($"Using Audience {audience} ");


            auth.AddJwtBearer("myscheme", options =>
            {
                options.Authority = realm; // "http://localhost:8080/auth/realms/kennyrealm";
                options.Audience = audience;
                options.RequireHttpsMetadata = false;
            });

            services.AddAuthorization(options =>
            {
                options.DefaultPolicy = new AuthorizationPolicyBuilder()
                    .AddAuthenticationSchemes(new[] { "myscheme" })
                    .RequireAuthenticatedUser()
                    .RequireRole("authorizedperson")

                    .Build();
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseDeveloperExceptionPage();
            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "server v1"));

            app.UseRouting();

            app.UseCors(builder =>
                builder.AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials()
                    .SetIsOriginAllowed(s => true));

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
