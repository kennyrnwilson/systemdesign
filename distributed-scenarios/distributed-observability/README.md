# Distributed Observability
Use docker-compose to start up a a distributed system to highlight the three pillars of Observability.

- ElasticSearch: http://localhost:9200
- Kibana: http://localhost:5601
- LogStash (HTTP): http://localhost:31311
- .NET Core Web API: http://localhost:8100/swagger/index.html
- Web Client - React SPA: http://localhost:3000

## StartUp/TakeDown
To start up the stack run the following command
```
docker-compose up --build
```

Because the elasticsearch docker image uses volumes to store data by default, if we want to cleaup the stack we use the following command

```
docker-compose down -v
```
## Configure Logstash
The following steps show how to configure logstash

### 1. Open Disover 
Open the URL http://localhost:5601 and then select Discover from the left hand side 

![Discover](./docs/images/discover.png "Disover Tab")

The tab will ask you to "Create Index Pattern" so click the button and enter the patter **"logstash-*"** then select a Time field and you are done. I selected @timestamp. Finally hit the "Create Index pattern button

### 2. Click Create Pattern button

![Create Index Pattern](./docs/images/create_index_pattern.png "Create Index pattern")

Now from the discover tab we should have something like the following. We have no available fields as we still need to publish something

### 3. Define index pattern
Enter the index pattern logstash-* and click next step

![Define Index Pattern](./docs/images/define_index_pattern.png "Define Index pattern")

### 4. Configure primary time field

![Configure Time Field](./docs/images/configure-primary-time-field.png "Configure Time Field")

### 5. Add fields
Now we can specify the fields we want to see

![Selected Fields](./docs/images/choose_fields.png "Selected Fields")

### 6. Check the fields in logstash
Because the .NET API started publishing to LogStash as soon as it came up we already have messages in the log so we can see a set of fields

![Log Fields](./docs/images/log_fields.png "Log Fields")


### 7. Open the Discover tab again
![Discover](./docs/images/discover.png "Disover Tab")

### 8. Enter a log entry
Execute the Swagger endpoint

 http://localhost:8100/swagger/index.html

and click the refresh button. You should see log entries.  

![Log Entries](./docs/images/log_entries.png "Log Entries")

### 9. Configure what we want to see


![Log](./docs/images/log.png "Log")