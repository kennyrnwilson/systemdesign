import React, { ReactElement } from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { init as initApm } from '@elastic/apm-rum'


const apm = initApm({

  // Set required service name (allowed characters: a-z, A-Z, 0-9, -, _, and space)
  serviceName: 'React SPAnpm ',

  // Set custom APM Server URL (default: http://localhost:8200)
  serverUrl: 'http://localhost:8200',

  // Set service version (required for sourcemap feature)
  serviceVersion: ''
})

const t = apm.getCurrentTransaction();
if (t)
{
  t.name = "Hello";
}


async function DoWork() {
  const result = await fetch('http://localhost:8100/weatherforecast',
    {
      mode: "cors",
    }
  );

  ReactDOM.render(
    <React.StrictMode>
      <App json={await result.json()}></App>
    </React.StrictMode>,
    document.getElementById('root')
  );

  t?.end();
}


function App(props:any) : ReactElement
{
  return <pre>{JSON.stringify(props.json,null, 2)}</pre>
}

DoWork();
