﻿using System.IO;
using Google.Protobuf.Examples.AddressBook;
using Google.Protobuf;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            MyType type = new MyType()

            {
                MyDouble = 3.141592653589,
                MyInt = 1234567899,
                MyString = "A reasonably long string"
            };

            using (var output = File.Create("mytype.dat"))
            {
                type.WriteTo(output);
            }
        }
    }
}
