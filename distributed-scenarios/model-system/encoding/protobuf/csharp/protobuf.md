# Protocol Buffers

## C#

This project shows how to create simple protocol buffer definition files, compile then to C# code and then serialize and deserialize objects using the generated C# code. It is based loosely on the following [Google Tutorial](https://developers.google.com/protocol-buffers/docs/csharptutorial). We also provide a Dockerfile build and run the samples on docker so if we are building a more complete system we have this angle covered. 

The project file of the project that builds the proto files need the following content. See [ProtoBufferLibrary.csproj](./ProtoBuffer/ProtoBufferLibrary/ProtoBufferLibrary.csproj)

```
  <ItemGroup>
    <PackageReference Include="Google.Protobuf" Version="3.17.0" />
    <PackageReference Include="Grpc.Tools" Version="2.37.1">
      <PrivateAssets>all</PrivateAssets>
      <IncludeAssets>runtime; build; native; contentfiles; analyzers; buildtransitive</IncludeAssets>
    </PackageReference>
  </ItemGroup>

  <ItemGroup>
    <Protobuf Include="../**/*.proto" Access="Public" />
  </ItemGroup>
```

The code that takes a C# object and serializes it to file looks as follows. See [Program.cs](./ProtoBuffer/ConsoleApp/Program.cs)

```cs
MyType type = new MyType()
    {
        MyDouble = 3.141592653589,
        MyInt = 1234567899,
        MyString = "A reasonably long string"
    };

using (var output = File.Create("mytype.dat"))
{
    type.WriteTo(output);
}
```

### Docker
To build the docker image 

```docker build -t proto .
```
If you want to run exe and check the file inside the docker container and open a terminal onto the container.

```
docker run -d proto sleep 3000
```
