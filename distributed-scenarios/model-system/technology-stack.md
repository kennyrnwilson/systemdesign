# Technology Stack

## Tools
 * [Postman](./tools/postman/docs/postman.md) - HTTP Client for executing endpoints

## Used Technologies

### Frameworks
 * [.NET Core](./dotnetcore/docs/dotnetcore.md) 

### Observability
 * [ELK+APM](./elk/docs/elk.md) 
 * [Prometheus](./prometheus/docs/prometheus.md)

### Data Stores
 * [MongoDB](./mongodb/docs/mongodb.md)


### Security
- [KeyCloak](https://www.keycloak.org/)
- [JWT](https://jwt.io/)
- [OAuth](https://oauth.net/2/)
- [OpenId Connect](https://openid.net/connect/)

### Inter Process Communication and API formats
- [REST](https://developer.mozilla.org/en-US/docs/Glossary/REST)


## Bucket List
### Actor Framework
An Actor based framework supports highly concurrent, distributed, thread-safe applications. Microsoft has the Orleans framework [Orleans]()

### Service Discovery
[Consul](https://www.consul.io/) enables one service to automatically find health instances of other services without needing to know about ports and DNS aliases. 

 * Service Deployment [Nomad](https://www.nomadproject.io/)

### Secrets Management

[Vault](https://www.vaultproject.io/) enables one to places all ones secrets in a single location

### Data Stores
  - SQL Server
    - In-memory OLTP
  - Kafka
  - Redis

### Frameworks
- Node.JS Web API
    - Rest
    - GraphQL

## Inter Process Communication and API formats
- [GraphQL](https://graphql.org/)
- [Swagger](https://swagger.io/)
- [NSwag](https://github.com/RicoSuter/NSwag)
- [Protocal Buffer](https://developers.google.com/protocol-buffers)
- [SignalR](https://dotnet.microsoft.com/apps/aspnet/signalr)

