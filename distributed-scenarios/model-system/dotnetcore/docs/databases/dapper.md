# Dapper
[Dapper](https://github.com/DapperLib/Dapper) is a high performance, lightweight ORM.

 * [LearnDapper](https://www.learndapper.com/)
 * [Dapper Tutorial](https://dapper-tutorial.net/) 

For an example of using Dapper in a working fragment of code see [ProductTypeRepository.cs](../../webapi/SQLServer/ProductTypeRepository.cs) 
