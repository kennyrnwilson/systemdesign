# Swagger

To quote from the [The OpenAPI Specification](https://swagger.io/specification/)

 "The OpenAPI Specification (OAS) defines a standard, language-agnostic interface to RESTful APIs which allows both humans and computers to discover and understand the capabilities of the service without access to source code, documentation, or through network traffic inspection."


[Swagger](https://swagger.io/) is a set of tools to help with REST API building. The Swagger site has documentation on [best practices](https://swagger.io/resources/articles/best-practices-in-api-design/) when building Open APIs

## Swashbuckle
 [Swashbuckle](https://docs.microsoft.com/en-us/aspnet/core/tutorials/getting-started-with-swashbuckle?view=aspnetcore-5.0&tabs=visual-studio) adds swagger support to .NET core. 

In our particular use case we setup our swagger to support JWT bearer tokens

```cs
        public static void AddSwaggerWithJwtBearer(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                // The basic Swagger support
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "server", Version = "v1" });

                // Enable the user to add a bearer token in the Swagger Front End
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    In = ParameterLocation.Header,
                    Description = "Enter the following: Bearer Xax123....",
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        new string[] { }
                    }
                });
            });
        }
    }
```

For the full code see [Startup.cs](../webapi/webapi/Startup.cs)


