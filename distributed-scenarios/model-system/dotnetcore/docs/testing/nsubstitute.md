# NSubstitute - Mocking

For mocking I use [NSubstitute](https://nsubstitute.github.io/). For examples of using NSubstitute see [UsingNSubstitute.cs](../../webapi/UnitTests/UsingNSubstitute.cs)

