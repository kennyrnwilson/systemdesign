# AutoFixture -  

For more complex scenarios it is often useful to use [AutoFixture](https://github.com/AutoFixture/AutoFixture) . For an example see [UsingAutoFixture.cs](../../webapi/UnitTests/UsingAutoFixture.cs). Note to make this work we need to create an attribute called [AutoSubstitute](../../webapi/UnitTests/AutoSubstituteAttribute.cs)

