# .NET Core

**Testing**
   - [XUnit](./testing/xunit.md)
   - [NSubstitute](./testing/nsubstitute.md)
   - [AutoFixture](./testing/autofixture.md)
   - [Alba](./testing/alba.md)

**Misc**
 - [Serilog](https://serilog.net/) Structured Logging
 - [Swagger](https://swagger.io/)
 - [Elastic APM](https://www.elastic.co/apm) Application Monitoring: 
 - [HealthChecks](https://docs.microsoft.com/en-us/aspnet/core/host-and-deploy/health-checks?view=aspnetcore-5.0)

**Databases**
- [System.Data.SqlClient](https://docs.microsoft.com/en-us/dotnet/api/system.data.sqlclient?view=net-5.0)
 - [Dapper](./databases/dapper.md)


 **Libraries**
- TPL DataFlow
- RX
- GrapQL:
