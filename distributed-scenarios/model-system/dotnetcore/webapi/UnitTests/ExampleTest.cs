using Xunit;

namespace UnitTests
{
    // In XUnit tests can be in any class. We have no TestFixture. 
    public class TestClass
    {
        // Facts test invariants. Things which should always be true
        [Fact]
        public void HelloFact() => Assert.Equal(2,1+1);

        // Theories are true only for certain sets of data 
        [Theory]
        [InlineData(1)]
        [InlineData(3)]
        public void HelloTheory(int x) => Assert.True(x % 2 ==1);
    }
}
