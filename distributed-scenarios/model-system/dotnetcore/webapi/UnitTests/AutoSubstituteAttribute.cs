﻿using AutoFixture;
using AutoFixture.AutoNSubstitute;
using AutoFixture.Xunit2;

namespace UnitTests
{
    public class AutoSubstituteAttribute : AutoDataAttribute
    {
        public AutoSubstituteAttribute() : base(()=>new Fixture().Customize(new AutoNSubstituteCustomization())) { }
    }
}
