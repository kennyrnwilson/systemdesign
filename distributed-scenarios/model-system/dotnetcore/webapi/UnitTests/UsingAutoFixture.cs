﻿using AutoFixture;
using Xunit;

namespace UnitTests
{
    public class UsingAutoFixture
    {
        [Theory, AutoSubstitute]
        public void ShowAutoFixture(Fixture fixture)
        {
            var sut = fixture.Create<SuT>();
        }
    }

    public interface IUnusedDep { }

    public class SuT
    {
        public SuT(IUnusedDep uniUnusedDep) { }
    }
}
