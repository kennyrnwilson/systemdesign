﻿using NSubstitute;
using Xunit;
namespace UnitTests
{
    public class UsingNSubstitute
    {
        [Fact]
        public void TestAdd()
        {
            var mock = Substitute.For<ICalculator>();
            mock.Add(Arg.Any<int>(), Arg.Any<int>()).Returns(5);

            Assert.Equal(5,mock.Add(3,2));
        }
    }

    public interface ICalculator
    {
        int Add(int x, int y);
    }
}
