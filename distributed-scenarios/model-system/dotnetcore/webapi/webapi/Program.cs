using System;
using Elastic.CommonSchema.Serilog;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Sinks.Elasticsearch;

namespace ModelSystem.WebAPI.WebAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); })
                .ConfigureLogging((hostingContext, logging) =>
                {
                    var elasticUrl = hostingContext.Configuration.GetValue<string>("ElasticsearchUrl");

                    Log.Logger = new LoggerConfiguration()
                        // Read what we can from appsettings.json
                        .ReadFrom.Configuration(hostingContext.Configuration)

                            // Configure elastic in code as its such a pain in config
                            .WriteTo.Elasticsearch(new ElasticsearchSinkOptions(new Uri(elasticUrl))
                            {
                                CustomFormatter = new EcsTextFormatter()
                            })


                        .CreateLogger();
                })
                .UseSerilog();
    }
}
