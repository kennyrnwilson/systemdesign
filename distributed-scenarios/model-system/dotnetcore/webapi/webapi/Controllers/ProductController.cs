﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ModelSystem.WebAPI.Products;
using ModelSystem.WebAPI.Products.Services;

namespace ModelSystem.WebAPI.WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductController : ControllerBase
    {
        private readonly ILogger<ProductController> _logger;
        private readonly IProductTypeRepository _productTypeRepository;
        private readonly IOptionRepository _optionRepository;

        public ProductController(ILogger<ProductController> logger, IProductTypeRepository productTypeRepository, IOptionRepository optionRepository)
        {
            _logger = logger;
            _productTypeRepository = productTypeRepository;
            _optionRepository = optionRepository;
        }

        [HttpGet]
        [Authorize]
        public Task<IEnumerable<ProductType>> Get()
        {
            _logger.LogInformation($"{this.GetType().Name},{nameof(Get)}");

            var productTypes =_productTypeRepository.GetAllProductTypes();
            return productTypes;
        }

        [HttpGet("All")]
        [Authorize]
        public async Task<AllData> GetAllData()
        {

            var productTypes = await _productTypeRepository.GetAllProductTypes();
            var options = await _optionRepository.GetAllOptions();

            return new AllData() {Options = options, ProductTypes = productTypes};
        }


        [HttpGet("GetOptions")]
        [Authorize]
        public Task<IEnumerable<Option>> GetOptions()
        {
            _logger.LogInformation($"{this.GetType().Name},{nameof(GetOptions)}");

            var options = _optionRepository.GetAllOptions();
            return options;
        }
    }
}
