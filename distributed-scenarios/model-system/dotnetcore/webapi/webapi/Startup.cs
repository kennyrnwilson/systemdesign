using System;
using System.Collections;
using System.Net.Http;
using System.Threading.Tasks;
using Elastic.Apm;
using Elastic.Apm.AspNetCore;
using Elastic.Apm.SqlClient;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using ModelSystem.WebAPI.MongoDb;
using ModelSystem.WebAPI.Products.Services;
using ModelSystem.WebAPI.SQLServer;
using MongoDB;
using Prometheus;

namespace ModelSystem.WebAPI.WebAPI
{
    public class Startup
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _webHostEnvironment;
        private ILogger<Startup> _logger;
        private string _keyCloakRealm;
        private string _keyCloakAudience;

        public Startup(IConfiguration configuration, IWebHostEnvironment webHostEnvironment)
        {
            _configuration = configuration;
            _webHostEnvironment = webHostEnvironment;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddProductApi();

            // Our clients will always have a different origin so we always need to 
            // support CORS
            services.AddCors();

            // Add the actual endpoint actions
            services.AddControllers();

            // We use swagger in all environments. 
            services.AddSwaggerWithJwtBearer();

            // Setup KeyCloak 
            _keyCloakRealm = _configuration.GetValue<string>("KeyCloak:Realm");
            _keyCloakAudience = _configuration.GetValue<string>("KeyCloak:Audience");
            services.AddKeyCloakAuth(_configuration, _keyCloakRealm,_keyCloakAudience);

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILogger<Startup> logger)
        {
            _logger = logger;

            // Log out the environment we are running under. 
            logger.LogInformation($"Environment: {env.EnvironmentName}");

            // Useful, especially if we are running inside docker. 
            if (_webHostEnvironment.EnvironmentName == "Development") PrintEnvironmentVariables();

            // Log out the KeyCloak details
            LogKeyCloakDetails();

            app.UseDeveloperExceptionPage();

            // Add APM Application Performance Monitoring
            app.AddApm(_configuration);
            Agent.Subscribe(new SqlClientDiagnosticSubscriber());

            app.UseSwagger();

            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "server v1"));

            app.UseRouting();

            app.UseCors(builder =>
                builder.AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials()
                    .SetIsOriginAllowed(s => true));

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapMetrics();
            });
        }

        /// <summary>
        /// A method that helps debugging. 
        /// </summary>
        private void PrintEnvironmentVariables()
        {
            foreach (DictionaryEntry environmentVariable in Environment.GetEnvironmentVariables())
            {
                _logger.LogDebug($"{environmentVariable.Key} : {environmentVariable.Value}");
            }
        }

        private void LogKeyCloakDetails()
        {
            var keyCloak = new { Audience = _keyCloakAudience, Realm = _keyCloakRealm };

            _logger.LogInformation($"KeyCloak Authority {keyCloak} ");
        }
    }

    static class ExtensionMethods
    {
        public static void AddProductApi(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddSingleton<IProductTypeRepository, ProductTypeRepository>();
            serviceCollection.AddSingleton<IMongoConnectionManager, MongoConnectionManager>();
            serviceCollection.AddSingleton<IOptionRepository, OptionRepository>();
        }

        public static void AddApm(this IApplicationBuilder app, IConfiguration configuration)
        {
            app.UseElasticApm(configuration);
        }

        public static void AddKeyCloakAuth(this IServiceCollection services, IConfiguration configuration, string realm, string audience)
        {
            if (configuration.GetValue<bool>("AllowAnonymous"))
            {
                services.AddSingleton<IAuthorizationHandler, AnonymousAuthorizationHandler>();
                return;
            }

            var auth = services.AddAuthentication();

            auth.AddJwtBearer("myscheme", options =>
            {
                // Special Logic that tells us not to check the certificate of the 
                // Authorization server. Not recommended in production
                if (configuration.GetValue<bool>("DoNotValidateAuthServerCert"))
                {
                    options.BackchannelHttpHandler = new HttpClientHandler
                    {
                        ServerCertificateCustomValidationCallback = (_, _, _, _) => true
                    };
                }

                options.Authority = realm; 
                options.Audience = audience;
                options.RequireHttpsMetadata = false;
            });

            services.AddAuthorization(options =>
            {
                options.DefaultPolicy = new AuthorizationPolicyBuilder()
                    .AddAuthenticationSchemes(new[] { "myscheme" })
                    .RequireAuthenticatedUser()
                    .RequireRole("authorizedperson")
                    .Build();
            });
        }

        /// <summary>
        /// Add Swagger support to the Web API. As we secure using KeyCloak we need
        /// enable Swagger users to enter bearer tokens which is what we do here. 
        /// </summary>
        /// <param name="services">The services collection</param>
        public static void AddSwaggerWithJwtBearer(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                // The basic Swagger support
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "server", Version = "v1" });

                // Enable the user to add a bearer token in the Swagger Front End
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    In = ParameterLocation.Header,
                    Description = "Enter the following: Bearer Xax123....",
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        new string[] { }
                    }
                });
            });
        }
    }

    public class AnonymousAuthorizationHandler : IAuthorizationHandler
    {
        public Task HandleAsync(AuthorizationHandlerContext context)
        {
            foreach (var requirement in context.PendingRequirements)
            {
                context.Succeed(requirement);
            }

            return Task.CompletedTask;
        }
    }
}
