using System;
using System.Threading.Tasks;
using Alba;
using ModelSystem.WebAPI.Products;
using ModelSystem.WebAPI.WebAPI;
using Xunit;

namespace IntegrationTesting
{
    public class AlbaEndPointTest
    {
        [Fact]
        public async Task Test1()
        {
            Environment.SetEnvironmentVariable("AllowAnonymous", "true");

            using var system = SystemUnderTest.ForStartup<Startup>();

            var result = await system.GetAsJson<AllData>("/Product/All");
        }
    }
}
