﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ModelSystem.WebAPI.Products;
using ModelSystem.WebAPI.Products.Services;
using Dapper;

namespace ModelSystem.WebAPI.SQLServer
{
    public class ProductTypeRepository : IProductTypeRepository
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<ProductTypeRepository> _logger;
        private readonly SqlConnection _sqlConnection; 

        public ProductTypeRepository(IConfiguration configuration, ILogger<ProductTypeRepository> logger)
        {
            _configuration = configuration;
            _logger = logger;

            var connectionString = configuration.GetConnectionString("ProductDatabase");
            logger.LogInformation($"Connection string is {connectionString}");
            _sqlConnection = new SqlConnection(connectionString);
            _sqlConnection.Open();
        }

        public async Task<IEnumerable<ProductType>> GetAllProductTypes()
        {
            var procedureName = "GetAllProductTypes";
            var values = new { };
            var results = _sqlConnection.Query<ProductType>(procedureName, values, commandType: CommandType.StoredProcedure).ToList();

            return results;
        }
    }
}
