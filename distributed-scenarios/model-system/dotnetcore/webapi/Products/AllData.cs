﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelSystem.WebAPI.Products
{
    public class AllData
    {
        public IEnumerable<Option> Options { get; set; }
        public IEnumerable<ProductType> ProductTypes { get; set; }
    }
}
