﻿namespace ModelSystem.WebAPI.Products
{
    public class Option
    {
        public string Ticker { get; set; }
        public int Id { get; set; }
    }
}
