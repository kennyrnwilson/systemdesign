﻿namespace ModelSystem.WebAPI.Products
{
    public class ProductType
    {
        public int Id { get; init; }

        public string Description { get; init; }
    }
}
