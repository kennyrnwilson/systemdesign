﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ModelSystem.WebAPI.Products.Services
{
    public interface IProductTypeRepository
    {
        Task<IEnumerable<ProductType>> GetAllProductTypes();
    }
}
