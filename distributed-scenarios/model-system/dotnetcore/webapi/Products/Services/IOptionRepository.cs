﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ModelSystem.WebAPI.Products.Services
{
    public interface IOptionRepository
    {
        Task<IEnumerable<Option>> GetAllOptions();
    }
}
