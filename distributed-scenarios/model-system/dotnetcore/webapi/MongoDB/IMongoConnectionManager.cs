﻿using MongoDB.Driver;

namespace ModelSystem.WebAPI.MongoDb
{
    public interface IMongoConnectionManager
    {
        IMongoCollection<TCollection> GetMongoCollection<TCollection>(string collectionName);
    }
}
