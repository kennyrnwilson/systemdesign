﻿using System.Linq;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;

namespace ModelSystem.WebAPI.MongoDb
{
    public class MongoConnectionManager : IMongoConnectionManager
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<MongoConnectionManager> _logger;
        private readonly MongoClient _client;
        private readonly string _databaseName;

        public MongoConnectionManager(IConfiguration configuration, ILogger<MongoConnectionManager> logger)
        {
            _configuration = configuration;
            _logger = logger;

            var uri = configuration.GetConnectionString("Mongo:Uri");
            var settings = MongoClientSettings.FromConnectionString(uri);
            
            _databaseName = configuration.GetConnectionString("Mongo:Database");
            _client = new  MongoClient(uri);

            _logger.LogInformation($"Mongo Uri {uri}");
            _logger.LogInformation($"Mongo Uri {_databaseName}");

            ConventionRegistry.Register("My Custom Conventions", new ConventionPack
            {
            
                new CamelCaseElementNameConvention(),
                new IgnoreExtraElementsConvention(true),
                new IgnoreIfDefaultConvention(true),
                new EnumRepresentationConvention(BsonType.String)
            }, t => { return t.FullName != null && t.FullName.StartsWith(GetType().Namespace.Split(".").First()); });
        }

        public IMongoCollection<TCollection> GetMongoCollection<TCollection>(string collectionName) =>
            _client.GetDatabase(_databaseName).GetCollection<TCollection>(collectionName);
    }
}