﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ModelSystem.WebAPI.Products;
using ModelSystem.WebAPI.Products.Services;
using MongoDB.Driver;

namespace ModelSystem.WebAPI.MongoDb
{
    public class OptionRepository : IOptionRepository
    {
        private readonly IMongoConnectionManager _connectionManager;

        public OptionRepository(IMongoConnectionManager connectionManager)
        {
            _connectionManager = connectionManager;
        }
        
        public async Task<IEnumerable<Option>> GetAllOptions()
        {
            var collection = _connectionManager.GetMongoCollection<Option>("options");
            var documents = await collection.Find(_ => true).ToListAsync();
            return documents;
        }
    }
}
