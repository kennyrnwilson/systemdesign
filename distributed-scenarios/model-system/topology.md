# The Distributed Model Application

## Run 
To run up the whole system is just a case of executing the following command from the project root directory. This is the folder that container the file docker-compose.yml. 

```
docker-compose up --build
```

We can take down and cleanup the system using the command

```
docker-compose down -v
```
We know the system is fully up when we can access the Kibana and Keycloak endpoints. This can take a few minutes.

 - [KeyCloak](http://localhost:8080/auth/)
 - [Kibana](http://localhost:5601/app/home#/)

Once the system comes up our basic topology is as follows 
<br><br>

## Deployment Topology 
The following diagram shows how the nodes in the topology are wired together. 
<br>

![Add Auth](./docs/imgs/topology.png "Add Auth")


**KeyCloak Auth Server**


| Key           | Description                 |
| -----------   | ----------------------------|
| URL           | http://localhost:8080       |
| Client        | testclient       |
| Admin User    | admin                       |
| Admin Pword   | admin                       |
| User          | testuser                    |
| Password      | testuser                    | 

<br><br>

**ELK + APM**

For observability we use the [ELK stack](https://www.elastic.co/what-is/elk-stack) with [APM](https://www.elastic.co/apm) 


| Key           | Description                 |
| -----------   | ----------------------------|
| ElasticSearch | http://localhost:9200       |
| Kibana        | http://localhost:5601       |
| LogStash      | http://localhost:31311      |
| APM           | http://localhost:8200       |
| User          | testuser                    |
| Password      | testuser                    | 

<br><br>

**Prometheus**

| Key             | Description                   |
| -----------     | ----------------------------  |
| Server          | http://localhost:9090/        |
| WebApi Endpoint | http://localhost:8050/metrics |

<br><br>

**.NET Web API**

| Key           | Description                              |
| -----------   | -------------------------------          |
| Product       | http://localhost:8050/Product            |
| Swagger       | http://localhost:8050/swagger/index.html |
| Prometheus    | http://localhost:8050/metrics            |

<br><br>

**React.js SPA Front End**

| Key           | Description                              |
| -----------   | -------------------------------          |
| URI           | http://localhost:3000                    |
| Swagger       | http://localhost:8050/swagger/index.html |

<br><br>

**SQL Server**

| Key           | Description                 |
| -----------   | ----------------------------|
| URL           | http://localhost:1433       |
| Client        | testclient                  |
| Admin Login   | sa                          |
| Admin Pword   | Kw!Kw!Kw!                   |
| User Login    | user                        |
| User Pword    | user                        | 

<br><br>

**MongoDB**
| Key           | Description                 |
| -----------   | ----------------------------|
| URL           | http://localhost:27010      |

