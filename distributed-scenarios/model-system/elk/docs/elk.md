# ELK Stack
## What is ELK Stack
For observability we use the Elastic ELK+APM stack. See the [Elastic Observability documentation](https://www.elastic.co/guide/en/observability/current/index.html) for details. 

### Features

### Components

### Archirecture

## More Documentation
 - [Elastic](https://www.elastic.co/)
 - [Elastic APM](https://www.elastic.co/apm)
 - [Elastic Logstash](https://www.elastic.co/logstash)
 - [Elastic Kibana](https://www.elastic.co/kibana)

