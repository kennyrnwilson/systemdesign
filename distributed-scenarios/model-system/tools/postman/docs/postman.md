# Postman
[Postman](https://www.postman.com/) is a super useful tool that enables one to send http requests and execute REST endpoints. This folder contains a file that can be used to load up a Postman configuration that executes the REST endpoints provided in this projects model distributed system. Once you have installed and opened Postman you can import this configuration as follows. 

**Configuring Postman**

![Add Auth](./imgs/postman_collection.png "Add Auth")

The saved configuration has pre-request logic to obtain a bearer token which it inserts into the actual HTTP requests. The following image shows executing a single endpoint of the .NET core WebAPI endpoint.

<br>

**Send Request**

![Add Auth](./imgs/postman_send.png "Send Request")
