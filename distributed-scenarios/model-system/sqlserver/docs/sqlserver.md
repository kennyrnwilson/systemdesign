# SQLServer
## Overview
### What is SQLServer?
Relational Database from Microsoft

#### Features


## More Documentation
 - [SQL Server Documentation](https://docs.microsoft.com/en-us/sql/sql-server/?view=sql-server-ver15)


## Setup
 For this project we use docker to bring up the instance of SQLServer, initialise a database and populate it with data. 

### In-Memory OLTP
To setup In-memory OLTP we add the following fragment of code

**Add In-memory support**
```sql
ALTER DATABASE [Products]
ADD FILEGROUP Products_Filegroup CONTAINS MEMORY_OPTIMIZED_DATA;

ALTER DATABASE [Products]
ADD FILE (
    NAME=InMemoryProducts,
    FILENAME='/var/opt/mssql/data/inmemoryproducts'
)
TO FILEGROUP Products_Filegroup

```