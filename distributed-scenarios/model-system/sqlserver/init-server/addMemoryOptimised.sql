ALTER DATABASE [Products]
ADD FILEGROUP Products_Filegroup CONTAINS MEMORY_OPTIMIZED_DATA;

ALTER DATABASE [Products]
ADD FILE (
    NAME=InMemoryProducts,
    FILENAME='/var/opt/mssql/data/inmemoryproducts'
)
TO FILEGROUP Products_Filegroup

