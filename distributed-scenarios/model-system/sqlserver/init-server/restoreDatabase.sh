# echo "sleeping to allow db to start up"
 sleep 30

echo 'adding logins'
/opt/mssql-tools/bin/sqlcmd -S sql-server-instance -U 'SA' -P 'Kw!Kw!Kw!' -Q "CREATE LOGIN [user] WITH PASSWORD=N'user', DEFAULT_DATABASE=[master], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
"

echo 'enable logins'
/opt/mssql-tools/bin/sqlcmd -S sql-server-instance -U 'SA' -P 'Kw!Kw!Kw!' -Q "ALTER LOGIN [user] ENABLE"

echo 'restoring database Products'
/opt/mssql-tools/bin/sqlcmd -S sql-server-instance -U 'SA' -P 'Kw!Kw!Kw!' -Q "RESTORE DATABASE [Products] FROM DISK = N'/var/opt/mssql/data/Products.bak' WITH REPLACE"

echo 'associate kenny with login'
/opt/mssql-tools/bin/sqlcmd -S sql-server-instance -U 'SA' -P 'Kw!Kw!Kw!' -Q "use Products ALTER USER kenny WITH LOGIN = [user]"

echo 'adding memory optimized'
/opt/mssql-tools/bin/sqlcmd -S sql-server-instance -U 'SA' -P 'Kw!Kw!Kw!' -i ./addMemoryOptimised.sql