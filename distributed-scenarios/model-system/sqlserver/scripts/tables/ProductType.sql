USE [Products]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

if exists (select * from sys.objects where name = 'ProductType')
begin 
	drop table ProductType
end

CREATE TABLE [dbo].[ProductType](
	[id] [int] IDENTITY(1,1 )NOT NULL,
	[description] [nvarchar](32) NOT NULL
	CONSTRAINT [PK_ProductType] PRIMARY KEY CLUSTERED ([id] ASC)
) ON [PRIMARY]


GO


