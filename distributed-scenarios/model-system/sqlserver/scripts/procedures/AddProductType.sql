
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Kenny
-- Create date: 19/05/2021
-- Description:	Add New ProductType
-- =============================================
CREATE PROCEDURE AddProductType
	-- Add the parameters for the stored procedure here
	@id int out,
	@description nvarchar(32)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	insert into ProductType (description) values (@description)

	select @id = SCOPE_IDENTITY()
END
GO
