# Model Distributed System
## Aim
The aim of this project is provide a knowledge repository for the technologies I feel are relevant for building a modern distributed application. The knowledge takes three forms.

 1. Static documentation and links to more details on the various technolgies.
 2. Real code that shows how to get started and use the technologies.
 3. Real distributed topology that starts up various processes in docker and wires then together.

The idea is that we can reduce systems building to choosing and fitting together different lego blocks. All the technologies used are chosen with a view to building applications with the following Non-Functional Requirements. 

+ Observability
+ Testability
+ Reliability 
+ High availability
+ Scalability
+ Security 
+ Evolvability
+ Maintainability

I favour open source, cross-platform technologies over proprietary or OS specific technologies. Some of the technologies 

## Server Topology Characteristics
Typically we would set up services in clusters that provide the following 

 * Support heterogenous front-end technologies including web and mobile
 * Centralised secret management and certificate rotation
 * Secure encrypted communications [Vault](https://www.vaultproject.io/)
 * Service name resolution and service discovery e.g. [Consul](https://www.consul.io/)
 * Rolling updates and blue/green deployments
 * Automatic scaling
 * Distributed Telemetry
 * Cross-platform actor based technology


 ### Security
 If all traffic between our services is encrypted using the same authority we can guarantee the identity of each and every service calling into other services. For this to work we require mutual TLS between our services with certificates distributed automatically to services. 

### Dapr
One technology I would like to investigate more is [Dapr](./dapr/docs/dapr.md)

In order to create instances of the various processes we make extensive use of [Docker](https://docs.docker.com/) . As part of this project I would also also like to investigate the use of [Ansible](https://www.ansible.com/)

As the project has two distinct purposes; static documentation and a live model system we link to two high level documents 

 + [Model System Topology](./topology.md)
 + [Technologies Used](./technology-stack.md)



For a list of the technologies we are using and aim to use see [Technologies Used](./TECH.md)


