# KeyCloak
We are using KeyCloak as our authorisation server. We expose both [http](http://localhost:8080) and [https](https://localhost:8443) endpoints. There are two users; **admin admin**, and **testuser testuser**.

One of the trickiest thing with KeyCloak is getting tokens. The following sections show how to get a token using PostMan and then how to use it in Swagger

## HOWTO

### Get Token
The simplest way to get a token from our keycloak server is to use PostMan. Use a Post command on the following URI 

http://localhost:8080/auth/realms/kennyrealm/protocol/openid-connect/token


![Get Token](./docs/img/postman.png "Get Token")

### Use token in Swagger

In swagger click the authorize button and copy the token from the swagger response. Excluse and surrounding quotes. Put the token in the box as follows

![Get Token](./docs/img/swagger_bearer.png "Get Token")


### Get Token and use it in one Postman call
First we add a global variable called **access_token** to postman. Right click eye in top right of postma where you see an eye icon and then add it to the global varaiables. Now in your request add a pre-request script as follows.

```
const tokenUrl = 'http://localhost:8080/auth/realms/kennyrealm/protocol/openid-connect/token';


const getTokenRequest = {
  method: 'POST',
  url: tokenUrl,
 header: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: {
          mode: 'urlencoded',
          urlencoded: [
            {key: "grant_type", value: "password", disabled: false},
              {key: "client_id", value: 'testclient', disabled: false},
            {key: "username", value: 'testuser', disabled: false},
            {key: "password", value: 'testuser', disabled: false}
        ]
      }
};

pm.sendRequest(getTokenRequest, (err, response) => {
  const jsonResponse = response.json();
  const newAccessToken = jsonResponse.access_token;
  pm.variables.set('access_token', newAccessToken);
});
```

Finally add the variable **access_token** to the authorization tab as follows 

![Add Auth](./docs/img/add_auth_to_postman.png "Add Auth")

### Export any changes made in management console
You need to export the data to the ./kcdata folder as follows.

**Note:** If you run the command and get errors it might be you need to change the **-Djboss.socket.binding.port-offset=100** to something other that 100

```
docker exec -it kc /opt/jboss/keycloak/bin/standalone.sh -Djboss.socket.binding.port-offset=100 -Dkeycloak.migration.action=export -Dkeycloak.migration.provider=singleFile -Dkeycloak.migration.realmName=kennyrealm -Dkeycloak.migration.usersExportStrategy=REALM_FILE -Dkeycloak.migration.file=/tmp/kcdata/kennyrealm-realm.json

```

If the above command does not copy the file over the volume to the host directory we can copy it as follows

```
docker cp kc:/tmp/kcdata/kennyrealm-realm.json .
```

