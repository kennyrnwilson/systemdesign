# Prometheus
## Overview
### What is Prometheus?
Open source systems monitoring and alerting toolkit

#### Features
Collection of time series happens using a pull model over HTTP

## More Documentation
 - [Overview](https://prometheus.io/docs/introduction/overview/)


 ## Setup
 ### .NET Core 
 So setup a .NET core client we need to add the following references to our .csproj file

 ```
    <PackageReference Include="prometheus-net" Version="4.1.1" />
    <PackageReference Include="prometheus-net.AspNetCore" Version="4.1.1" />
 ```

 Then to our Startup.cs we add a using statement for prometheus 

 ```cs
 using Prometheus;
 ```

Also in the Startup.cs Conffigure method we need to call MapMetrics

```cs
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapMetrics();
            });
```

If our .net web api is listening on port 8050 then the prometheus process exposes an endpoint of 

http://localhost:8050/metrics

Which the prometheus server can use to scrape. 

 ## Server Docker
 We run the server in docker. See the docker-compose.yml at the root of this project