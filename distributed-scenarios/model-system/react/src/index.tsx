import React, { ReactElement } from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Keycloak from 'keycloak-js';
import { ApmBase, init as initApm } from '@elastic/apm-rum'

const keycloak = Keycloak(`/config/keycloak.json?ts=${new Date().getTime()}`);

const apmEndpoint = (process.env.REACT_ENV == "Docker") ? "http://apm:8200" : "http://localhost:8200";
const apiEndpoint = (process.env.REACT_ENV == "Docker") ? "http://webapi:8050/Product/All" : "http://localhost:8050/Product/All";

async function InitializeApp() {

  

  const apm = initApm({
    serviceName: 'React SPAnpm ',
    serverUrl: apmEndpoint,
    serviceVersion: '',
    
    distributedTracingOrigins: ["http://localhost:8050"]
  });

  await keycloak.init(
    {
      onLoad: 'login-required',
      enableLogging: true,
      checkLoginIframe: false
    });

  const t = apm.getCurrentTransaction();
  if (t) {
    t.name = "Product Fetch";
  }

  await keycloak.updateToken(120);
  const token = keycloak.token;
  const tokenParsed = keycloak.tokenParsed;

  const result = await fetch(apiEndpoint,
    {
      mode: "cors",
      headers: [
        ['authorization', `Bearer ${keycloak.token}`]
      ]
    }
  );

  ReactDOM.render(
    <React.StrictMode>
      <App json={await result.json()}></App>
    </React.StrictMode>,
    document.getElementById('root')
  );
}


function App(props: any): ReactElement {
  return <pre>{JSON.stringify(props.json, null, 2)}</pre>
}

InitializeApp();
