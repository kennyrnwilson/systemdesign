
describe("Basic scenarios", () => {

    it("Check how many times a method is called", () => {
        const spy = jest.spyOn(myObject, "getValue");
        myObject.getValue();
        myObject.getValue();
        expect(spy).toBeCalledTimes(2);
    });

    it("Mock method", () => {
        const spy = jest.spyOn(myObject, "getValue");
        spy.mockReturnValue(10);
        expect(myObject.getValue()).toEqual(10);
    });

    it.each`
    x | y | z   
    ${1} | ${2} | ${3}
    ${12} | ${2} | ${14}
`("Same test logic and different parameters", ({ x, y, z }: { x: number, y: number, z: number }) => {
        expect(x + y).toBe(z);
    });


});

class MyClass {
    private _value: number = 0;
    setValue(value: number) { this._value = value; }
    getValue() { return this._value; }
}

const myObject: MyClass = new MyClass();

export { }