# Install Kafka on Linux
This document describes how to install and run Kafka on Ubuntu over WSL2 in windows. The first step is to ensure you have Java 8 installed. Type

## Install Kafka
**Install Java**
```
java -version
```

If you do not have this installed you will need to install version 8.

**Download Kafka**

From your home directory run the following command.

```
wget https://downloads.apache.org/kafka/2.8.0/kafka_2.12-2.8.0.tgz
```

You can then uncompress it using 

```
tar -xvf kafka_2.12-2.8.0.tgz
```
**Make sure it works**

From your home directory run the following command

```
 kafka_2.12-2.8.0/bin/kafka-topics.sh
```

**Add Kafka bin directory to your path**

From your root directory enter the command

```
code .bashrc
```
Add the following to the end of the .bashrc file 

```
export PATH=/home/kenne/kafka_2.12-2.8.0/bin:$PATH
```

You will need to open a new shell for the changes to .bashrc to take effect. 

## Run ZooKeeper

**Create directory to hold zookeeper data**

```
mkdir /home/kenne/kafka_2.12-2.8.0/data/zookeeper
```

**Tell Zookeeper to put data in this directory**

```
code kafka_2.12-2.8.0/config/zookeeper.properties
```

Set the dataDir in this file to be 

```
dataDir=/home/kenne/kafka_2.12-2.8.0/data/zookeeper
```

## Start ZooKeeper
From folder /home/kenne/kafka_2.12-2.8.0/ run

```
 zookeeper-server-start.sh config/zookeeper.properties
```

## Run Kafka
Make the directory 

```
mkdir /home/kenne/kafka_2.12-2.8.0/data/kafka
```

**Tell Kafka to put data in this directory**

```
code kafka_2.12-2.8.0/config/server.properties
```

**Start Kafka**

From /home/kenne/kafka_2.12-2.8.0
```
 kafka-server-start.sh  config/server.properties
```

## Using Kafka CLI
### Kafka-topics

Create a topic as follows. We cannot create a topic with a replication factor greater than 2.
```
kafka-topics.sh --zookeeper 127.0.0.1:2181 --topic kenny_topic --create --partitions 3 --replication-factor 1
```

Check the topic has been create

```
kafka-topics.sh --zookeeper 127.0.0.1:2181 --list

>> kenny_topic
```

Describe the topic that has been create
```
kafka-topics.sh --zookeeper 127.0.0.1:2181 --topic kenny_topic --describe

>> Topic: kenny_topic      TopicId: ozHTSjeKRM-6ZgHENtSN3Q PartitionCount: 3       ReplicationFactor: >>1    Configs:
>>        Topic: kenny_topic      Partition: 0    Leader: 0       Replicas: 0     Isr: 0
>>        Topic: kenny_topic      Partition: 1    Leader: 0       Replicas: 0     Isr: 0
>>        Topic: kenny_topic      Partition: 2    Leader: 0       Replicas: 0     Isr: 0
```

Delete the topic
```
kafka-topics.sh --zookeeper 127.0.0.1:2181 --topic kenny_topic --delete

>> kenny_topic
```