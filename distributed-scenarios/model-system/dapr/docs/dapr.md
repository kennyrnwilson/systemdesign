# Dapr
[Daps](https://dapr.io/)  is a small, standalone, cross-platform executable written in Go. It provides pluggable features accessed over HTTP/gRPC. It is open source and some of the features supported include 

 * Service discovery and invocation
 * Secrets and secure traffic with mTLS
 * Messaging
 * Telemetry via Open Telemetry
