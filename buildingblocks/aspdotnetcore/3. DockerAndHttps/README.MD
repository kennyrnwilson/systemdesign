# ASP.NET Core API over HTTPS

This block runs a .NET Core Web API over HTTPS. It is based on the following Microsoft article. 

[Hosting ASP.NET Core images with Docker over HTTPS](https://docs.microsoft.com/en-us/aspnet/core/security/docker-https?view=aspnetcore-5.0)

## Prerequisite
You must have a trusted development certificate in the folder

```
%USERPROFILE%\.aspnet\https
```

See [Generate self-signed certificates with the .NET CLI](https://docs.microsoft.com/en-us/dotnet/core/additional-tools/self-signed-certificates-guide) for infromation on how to create one.

## Build and Run

```
docker-compose up --build
```

## URL

https://localhost:8001/weatherforecast



