using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace ASPHelloWorld
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string message = args.Length > 0 ? args[0] : "Hello";
            var webHost = new WebHostBuilder()
                .UseKestrel()
                .Configure(app =>
                {
                    app.Run(async ctx =>
                    {
                        await ctx.Response.WriteAsync(message);
                    } );
                })
                .Build();

            webHost.Run();
        }
    }
}
