﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace ASPHelloWorld
{
    public class Startup
    {
        private async Task NonTerminatingMiddleware(HttpContext httpContext,
            Func<Task> next)
        {
            // Preprocessing the request
            await next();
            // Pose-process the request
        }

        private async Task TerminatingMiddleware(HttpContext httpContext)
        {
            _logger.LogInformation("TerminatingMiddleware");
            await httpContext.Response.WriteAsync("SeriLog Logging");
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILogger<Startup> logger)
        {
            _logger = logger;
            app.Use(NonTerminatingMiddleware);
            app.Run(TerminatingMiddleware);
        }

        private ILogger<Startup> _logger;
    }
}