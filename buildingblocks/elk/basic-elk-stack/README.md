# ELK
Use docker-compose to start up a simple ELK stack

- ElasticSearch: http://localhost:9200
- Kibana: http://localhost:5601
- LogStash (HTTP): http://localhost:31311

# Run
```
docker-compose up --build
```
# Cleanup
Because the elasticsearch docker image uses volumes to store data by default, if we want to cleaup the stack we use the following command

```
docker-compose down -v
```
# Logstash 

## Create Index
Open the URL http://localhost:5601 and then select Discover from the left hand side 

![Discover](docs/images/discover.PNG "Disover Tab")

The tab will ask you to "Create Index Pattern" so click the button and enter the patter "logstash-*" then select a Time field and you are done. I selected @timestamp. Finally hit the "Create Index pattern button

### Create Index Screen
![Create Index Pattern](./docs/images/create_index_pattern.PNG "Create Index pattern")

Now from the discover tab we should have something like the following. We have no available fields as we still need to publish something


![Create Index Pattern](./docs/images/empty_index.PNG "Create Index pattern")

## Publish
We can use the HTTP endpoint to send messages to logstash. In the following example we use a little bit of powershell to send the HTTP

```
$body = '{ 
    "user":"kennew",
     "detail": {
        "firstname" : "Rainbow",
        "secondname" : "Wilson"
     }
    }'

Invoke-WebRequest 'http://localhost:31311' -ContentType 'application/json' -Body $Body -Method 'PUT'
```

### Add fields
Now we can specify the fields we want to see

![Selected Fields](./docs/images/choose_fields.PNG "Selected Fields")

### Filter rows
We can filter rows using field names

![Filter Fields](./docs/images/filter_fields.PNG "Filter Fields")