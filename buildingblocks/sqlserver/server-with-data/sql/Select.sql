CREATE TABLE TelNumber
(
	Id INT NOT NULL,
	PersonId INT NOT NULL,
	Number VARCHAR(10) NOT NULL
)

INSERT INTO TelNumber
VALUES
(1,1, 02084357777),
(2,1, 07999321123),
(3,2, 0132456123),
(4,2, 07999321145)

--Basic Select
SELECT * 
FROM TelNumber

--Filtering Rows With Where
SELECT * 
FROM TelNumber
WHERE PersonId = 1

--Grouping
Select PersonId, COUNT(Number) AS 'Number Count'
FROM TelNumber
WHERE PersonId IN (1,2)
GROUp BY PersonId

--Having
Select PersonId, COUNT(Number) As 'Number Count'
FROM TelNumber
WHERE PersonId IN (1,2)
GROUP BY PersonId
HAVING COUNT(Number) > 0

--Order By
