# SQl Server + Data
This block creates an instance of SQL Server and loads up some test databases. The loaded databases include the databases for the Microsoft books

 - T-SQL Querying (PerformaceV3, TSQLV3)
 - T-SQL Fundamentals (TSQLV4)

You can use this block as an example of how to create a SQLServer node and initialize it with data 

## Run
```
docker-compose up --build
```




