echo "sleeping to allow db to start up"
sleep 30
echo 'restoring database TSQLV3'
/opt/mssql-tools/bin/sqlcmd -S sql-server-instance -U 'SA' -P 'Kw!Kw!Kw!' -Q "RESTORE DATABASE [TSQLV3] FROM DISK = N'/var/opt/mssql/data/TSQLV3.bak' WITH REPLACE"

echo 'restoring database PerformanceV3'
/opt/mssql-tools/bin/sqlcmd -S sql-server-instance -U 'SA' -P 'Kw!Kw!Kw!' -Q "RESTORE DATABASE [PerformanceV3] FROM DISK = N'/var/opt/mssql/data/PerformanceV3.bak' WITH REPLACE"

echo 'restoring database TSQLV4'
/opt/mssql-tools/bin/sqlcmd -S sql-server-instance -U 'SA' -P 'Kw!Kw!Kw!' -Q "RESTORE DATABASE [TSQLV4] FROM DISK = N'/var/opt/mssql/data/TSQLV4.bak' WITH REPLACE"

echo 'restoring database Select'
/opt/mssql-tools/bin/sqlcmd -S sql-server-instance -U 'SA' -P 'Kw!Kw!Kw!' -Q "RESTORE DATABASE [Select] FROM DISK = N'/var/opt/mssql/data/Select.bak' WITH REPLACE"

echo 'restoring database Joins'
/opt/mssql-tools/bin/sqlcmd -S sql-server-instance -U 'SA' -P 'Kw!Kw!Kw!' -Q "RESTORE DATABASE [Joins] FROM DISK = N'/var/opt/mssql/data/Joins.bak' WITH REPLACE"