CREATE PROCEDURE [dbo].[POSITION_UPSERT] 
(
    @positions [dbo].[POSITION_TABLE_TYPE] readonly
)
WITH NATIVE_COMPILATION, SCHEMABINDING
AS BEGIN ATOMIC WITH
(
    TRANSACTION ISOLATION LEVEL = SNAPSHOT, LANGUAGE = N'us_english'
)
DECLARE
    @I INT = 1,
    @ID INT,
    @SEC_ID INT

WHILE 
    @I > 0
BEGIN
    SELECT
        @ID = ID,
        @SEC_ID = SEC_ID
    FROM 
        @positions
    WHERE
        ROW_ID = @I


    IF @@ROWCOUNT=0
        SET @I = 0
    ELSE
    BEGIN
        UPDATE dbo.POSITION
            SET SEC_ID = @SEC_ID
            WHERE ID = @ID

        IF @@ROWCOUNT=0
            INSERT INTO dbo.POSITION
            (
                ID,
                SEC_ID
            )
            VALUES
            (
                @ID,
                @SEC_ID
            );
        SET @I += 1
    END
END
END
GO
