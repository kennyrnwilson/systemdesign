
-- Create the Database
CREATE DATABASE Positions
GO

-- Add support for In-memory OLTP
ALTER DATABASE [Positions]
ADD FILEGROUP Positions_Filegroup CONTAINS MEMORY_OPTIMIZED_DATA;

ALTER DATABASE [Positions]
ADD FILE 
(
    NAME=InMemoryPositions,
    FILENAME='/var/opt/mssql/data/inmemorypositions'
)
TO FILEGROUP Positions_Filegroup

ALTER LOGIN [SA] WITH DEFAULT_DATABASE = [Positions]