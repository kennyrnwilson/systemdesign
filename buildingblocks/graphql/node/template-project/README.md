# Graph QL Node.js
This project shows how to setup a very basic Node.js service in GraphQL. It forms a template which can 
be used for other projects. 

## Run (Local)
To run the project enter the following from the root directory

```
npm install
npm start
```

## Docker

### Build
```
docker build -t nodegraphql .
```

### Run

```
docker run -d -p 4000:4000  nodegraphql
```


 ## Entering queries
 From the url http://localhost:4000/graphql enter the following query

 ```
 query {
  
  fruit {
    name
    color
  },
  
  car {
    make
    model
  }
}
 ```
 
 ## Viewing the schema visually
 To visualize the schema go to the URL http://localhost:4000/voyager

## Code Structure

The Source Code has two classes 

### Server.ts
Boilderplate code that runs the server and middleware. Loads the GraphQL query definitons and resolvers from the QueryAndResolvers.ts file

### QueryAndResolvers.ts
Holds the query defintion and resolvers

