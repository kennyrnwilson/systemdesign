
export const queryDefinition = `
    type Fruit {
        name:String!
        color:String!
    }

    type Car {
        make: String!
        model: String!
    }

    type Query {
        fruit: Fruit!
        car: Car!
    }
`;

export const resolvers = {
    Query : {
        fruit() {
            return (
                {
                    name: "Apple",
                    color: "Red"
                }
            );
        },

        car() {
            return (
                {
                    make: "Ford",
                    model: "Fiesta"
                }
            )
        }
    }
};
