import express from 'express';
import {  ApolloServer, makeExecutableSchema } from 'apollo-server-express';
import { express as voyagerMiddleware } from 'graphql-voyager/middleware/index.js';
import { queryDefinition, resolvers } from './QuertAndResolvers';
import { GraphQLSchema } from 'graphql';

 async function startService() {

    const schema:GraphQLSchema = makeExecutableSchema({
        typeDefs: queryDefinition,
        resolvers: resolvers
    }
    );

    const server:ApolloServer = new ApolloServer({ schema });
    await server.start();
    const app = express();
    server.applyMiddleware({ app });
    app.use('/voyager', voyagerMiddleware({ endpointUrl: '/graphql' }));
    await new Promise(resolve => app.listen({ port: 4000 }));
    console.log(`http://localhost:4000${server.graphqlPath}`);
    return { server, app };
}

startService();
