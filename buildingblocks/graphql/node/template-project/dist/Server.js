"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const apollo_server_express_1 = require("apollo-server-express");
const index_js_1 = require("graphql-voyager/middleware/index.js");
const QuertAndResolvers_1 = require("./QuertAndResolvers");
async function startService() {
    const schema = apollo_server_express_1.makeExecutableSchema({
        typeDefs: QuertAndResolvers_1.queryDefinition,
        resolvers: QuertAndResolvers_1.resolvers
    });
    const server = new apollo_server_express_1.ApolloServer({ schema });
    await server.start();
    const app = express_1.default();
    server.applyMiddleware({ app });
    app.use('/voyager', index_js_1.express({ endpointUrl: '/graphql' }));
    await new Promise(resolve => app.listen({ port: 4000 }));
    console.log(`http://localhost:4000${server.graphqlPath}`);
    return { server, app };
}
startService();
//# sourceMappingURL=Server.js.map