"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.resolvers = exports.queryDefinition = void 0;
exports.queryDefinition = `
    type Fruit {
        name:String!
        color:String!
    }

    type Car {
        make: String!
        model: String!
    }

    type Query {
        fruit: Fruit!
        car: Car!
    }
`;
exports.resolvers = {
    Query: {
        fruit() {
            return ({
                name: "Apple",
                color: "Red"
            });
        },
        car() {
            return ({
                make: "Ford",
                model: "Fiesta"
            });
        }
    }
};
//# sourceMappingURL=QuertAndResolvers.js.map