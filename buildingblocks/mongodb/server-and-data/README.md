# MongoDB node loaded with data

A docker container that holds a running MongoDB instance loaded with some basic databases and data 

## Run
```
docker-compose up --build
```

## Connect

mongodb://localhost:27010




