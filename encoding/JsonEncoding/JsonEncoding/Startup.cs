using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace WebApplication1
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.Use(InspectBodyBytes);

            app.UseStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private async Task InspectBodyBytes(HttpContext ctx, Func<Task> f)
        {
            int bufferLength = 100;
            var s = ctx.Request.Body;

            byte[] buffer = new byte[bufferLength];
            int bytesRead = 0;

            while ((bytesRead = await s.ReadAsync(buffer, 0, bufferLength)) > 0)
            {
                StringBuilder hex = new StringBuilder(bytesRead * 2);

                for (int i = 0; i < bytesRead; i++)
                {
                    hex.AppendFormat("{0:x2}", buffer[i]).Append("x ");
                }
                System.Console.WriteLine(hex);
            }

            await f();
        }
    }
}
