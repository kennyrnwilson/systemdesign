﻿using Microsoft.AspNetCore.Mvc;

namespace JsonEncoding.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class JsonEncodingController : ControllerBase
    {
        [HttpPut]
        public void Put(IntegerWrapper integerWrapper)
        {
        }
    }

    public class IntegerWrapper
    {
        public int SomeLongIntegerFieldName { get; set; }
    }
}
